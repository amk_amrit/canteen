from django.contrib import admin
from .models import  Category,FoodItem,Order,OrderItem,Variation
from mptt.admin import MPTTModelAdmin,DraggableMPTTAdmin
# from treewidget.fields import TreeManyToManyField,TreeForeignKey

# Register your models here.


class CategoryAdmin(admin.ModelAdmin):
    
    prepopulated_fields = {'slug':('name',)}

admin.site.register(Category,CategoryAdmin)


class FoodItemAdmin(admin.ModelAdmin):
    list_display = ['name','price','created','updated']
    list_editable = ['price']
    list_filter = ['price','created','updated']
    search_fields = ['name','price']
    prepopulated_fields = {'slug':('name',)}


admin.site.register(FoodItem,FoodItemAdmin)



class OrderAdmin(admin.ModelAdmin):
    list_display = ['profile','order_created_time','order_created_date','paid']
    #prepopulated_fields = {'transction_id':('user',)}



admin.site.register(Order,OrderAdmin)


class OrderItemAdmin(admin.ModelAdmin):
    list_display = ['food_item','order_added_date','order_added_time']



admin.site.register(OrderItem,OrderItemAdmin)

admin.site.register(Variation)