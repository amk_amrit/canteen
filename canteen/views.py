from rest_framework import generics,viewsets
from .models import  Category,FoodItem,Order,OrderItem,Variation
from .serializers import (
     CategorySerializer,
     FoodItemSerializer,
     OrderSerializer,
     OrderItemSerializer,
     CategoryListSerializer,
     FoodItemListSerializer,
     OrderCreateSerializer,
     OrderItemListSerializer, 
     OrderDetailSerializer,
     FoodItemCreateSerializer,
     CategoryCreateSerializer,
     VariationSerializer,
     VariationUpdateSerializer,
     FoodCategorySerializer
)
from django.shortcuts import  get_object_or_404
from rest_framework.response import Response
from rest_framework.views import  APIView
from rest_framework.authentication import BasicAuthentication
from rest_framework.permissions import  IsAuthenticated,BasePermission,SAFE_METHODS
from django.http import  HttpResponse,JsonResponse
from django.views.decorators.csrf import  csrf_exempt
from rest_framework.parsers import  JSONParser
import json
from accounts.permissions import  IsSuperAdmin,IsStudent,IsKitchenStaff,IsAdmin
from rest_framework import status
from rest_framework.request import  Request 
from rest_framework.decorators import api_view,permission_classes
from rest_framework import permissions
from django.utils.decorators import method_decorator
from rest_framework.generics import  ListAPIView,CreateAPIView
from django.core.exceptions import ObjectDoesNotExist
from django_filters.rest_framework import DjangoFilterBackend
from rest_framework.filters import SearchFilter
import django_filters





class OrderItemFilter(django_filters.FilterSet):
    order_added_date = django_filters.DateFilter()
    order_added_date_gt = django_filters.DateFilter(field_name='order_added_date', lookup_expr='gt')
    order_added_date_lt = django_filters.DateFilter(field_name='order_added_date', lookup_expr='lt')
    total = django_filters.NumberFilter()
    total_gt = django_filters.NumberFilter(field_name='total',lookup_expr='gt')
    total_lt = django_filters.NumberFilter(field_name='total',lookup_expr='lt')
    profile__first_name = django_filters.CharFilter(lookup_expr='icontains')
    profile__last_name = django_filters.CharFilter(lookup_expr='icontains')

    class Meta:
        model = OrderItem
        fields = ['order_added_date','total','profile__first_name','profile__last_name']

class OrderFilter(django_filters.FilterSet):
    order_created_date = django_filters.DateFilter()
    order_created_date_gt = django_filters.DateFilter(field_name='order_created_date', lookup_expr='gt')
    order_created_date_lt = django_filters.DateFilter(field_name='order_created_date', lookup_expr='lt')
    grand_total = django_filters.NumberFilter()
    grand_total_gt = django_filters.NumberFilter(field_name='grand_total',lookup_expr='gt')
    grand_total_lt = django_filters.NumberFilter(field_name='grand_total',lookup_expr='lt')
    profile__first_name = django_filters.CharFilter(lookup_expr='icontains')
    profile__last_name = django_filters.CharFilter(lookup_expr='icontains')
    class Meta:
        model = Order
        fields = ['order_created_date','grand_total','profile__first_name','profile__last_name']

class ProductFilter(django_filters.FilterSet):
    name = django_filters.CharFilter(lookup_expr='icontains')
    category__name = django_filters.CharFilter(lookup_expr='icontains')
    price = django_filters.NumberFilter()
    price_gt = django_filters.NumberFilter(field_name='price', lookup_expr='gt')
    price_lt = django_filters.NumberFilter(field_name='price', lookup_expr='lt')
    class Meta:
        model = FoodItem
        fields = ['name','category__name']





class FoodItemView(APIView):
    permission_classes = (IsAdmin | IsSuperAdmin | IsKitchenStaff,)
    filter_backends = [SearchFilter]
    filter_fields = ['name']

    def get(self, request, format=None):
        queryset = FoodItem.objects.all()
        serializer = FoodItemListSerializer(queryset, many=True)
        context = {
            "status":200,
            "message":"All food item got successfully.",
            "food_item":serializer.data
        }
        return Response(context,status=200)

    def post(self, request, format=None):
        
        serializer = FoodItemSerializer(data=request.data)
        if serializer.is_valid():
            
            serializer.save()
            queryset = Category.objects.filter(id=serializer.data['category'])
            ser = FoodCategorySerializer(queryset,many=True)
            variation = Variation.objects.filter(id=serializer.data['variation'])
            ser1 = VariationSerializer(variation,many=True)
            #category = queryset[0]
            context = {
                "status":201,
                "message":"Food Item created successfully",
                "food_item":{
                    'id':serializer.data['id'],
                    'name':serializer.data['name'],
                    'slug':serializer.data['slug'],
                    'price':serializer.data['price'],
                    'image':serializer.data['image'],
                    'created' : serializer.data['created'],
                    'updated': serializer.data['updated'],
                    "category": ser.data[0],
                    'variation': ser1.data[0]
                }
            }
            return Response(context, status=status.HTTP_201_CREATED)
        else:
            context = {
                'status':400,
                "message":"Data entered is not valid",
                "errors":serializer.errors
            }
            return Response(context, status=status.HTTP_400_BAD_REQUEST)

class FoodItemDetailView(APIView):
    permission_classes = (IsAdmin | IsSuperAdmin | IsKitchenStaff,)
    def get(self,request,id,format=None):

        try:
            queryset = FoodItem.objects.filter(id=id)
            food_item = queryset[0]
        except:
            context = {
                "status":404,
                "message":f'Food item {id} does not exist.'
            }
            return Response(context,status=404)
        
        serializer = FoodItemListSerializer(queryset,many=True)
        context = {
            "status":200,
            "message":f'Food item {id} got successfully.',
            'food_item':serializer.data[0]
        }
        return Response(context,status=200)

    def delete(self,request,id,format=None):
        try:
            queryset = FoodItem.objects.filter(id=id)
            food_item = queryset[0]
        except:
            context = {
                "status":404,
                "message":f'Food item {id} does not exist.'
            }
            return Response(context,status=404)
        
        queryset.delete()
        context = {
            'status':200,
            'message':"Food Item deleted successfully"
        }
        return Response(context,status=200)


    
        
class FoodItemUpdateView(generics.UpdateAPIView):
    permission_classes = (IsAdmin | IsSuperAdmin | IsKitchenStaff,)
    queryset = FoodItem.objects.all()
    serializer_class = FoodItemCreateSerializer
    filter_backends = [DjangoFilterBackend]

    def update(self, request,id, *args, **kwargs):
        try:
            food_item = FoodItem.objects.get(id=id)
        except:
            context = {
                'status':404,
                "message":'food_item does not exists on database'
            }
            return Response(context,status=404)
        serializer = self.serializer_class(food_item, data=request.data,partial=True)
        if serializer.is_valid():
            serializer.save()
            queryset = Category.objects.filter(id=serializer.data['category'])
            ser = FoodCategorySerializer(queryset,many=True)
            variation = Variation.objects.filter(id=serializer.data['variation'])
            ser1 = VariationSerializer(variation,many=True)
            context = {
                'status':200,
                'message':"Updated food_item successfully.",
                'food_item':{
                    'id':serializer.data['id'],
                    'name':serializer.data['name'],
                    'slug':serializer.data['slug'],
                    'price':serializer.data['price'],
                    'image':serializer.data['image'],
                    'created' : serializer.data['created'],
                    'updated': serializer.data['updated'],
                    "category": ser.data[0],
                    'variation': ser1.data[0]
                }
            }
            return Response(context, status=status.HTTP_200_OK)
        else:
            context = {
                'status':400,
                'message':'Entered data is not valid',
                'error':serializer.errors
            }
            return Response(context,status=400)


class FoodItemFilter(viewsets.ModelViewSet):
    queryset = FoodItem.objects.all()
    serializer_class = FoodItemListSerializer
    filter_backends = [DjangoFilterBackend,SearchFilter]
    filter_class = ProductFilter
    
    permission_classes = (IsAdmin | IsSuperAdmin | IsKitchenStaff,)


   
   
class VariationView(APIView):
    permission_classes = (IsAdmin | IsSuperAdmin | IsKitchenStaff,)
    def get(self,request,format=None):
        queryset = Variation.objects.all()
        serializer = VariationSerializer(queryset,many=True)
        context = {
            'status':200,
            'message':"All variation got successfully",
            'variations':serializer.data
        }
        return Response(context,status=200)

    def post(self, request, format=None):
        
        serializer = VariationSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            context = {
                "status":201,
                "message":"Variation created successfully",
                "variation":serializer.data
            }
            return Response(context, status=status.HTTP_201_CREATED)
        else:
            context = {
                'status':400,
                "message":"Data entered is not valid",
                "errors":serializer.errors
            }
            return Response(context, status=status.HTTP_400_BAD_REQUEST)


class VariationDetailView(APIView):
    permission_classes = (IsAdmin | IsSuperAdmin | IsKitchenStaff,)
    def get(self,request,id,format=None):

        try:
            queryset = Variation.objects.filter(id=id)
            variation = queryset[0]
        except:
            context = {
                "status":404,
                "message":f'Variation {id} does not exist.'
            }
            return Response(context,status=404)
        
        serializer = VariationSerializer(queryset,many=True)
        context = {
            "status":200,
            "message":f'Variation {id} got successfully.',
            'variation':serializer.data[0]
        }
        return Response(context,status=200)
    
    

    def delete(self,request,id,format=None):
        try:
            queryset = Variation.objects.filter(id=id)
            variation = queryset[0]
        except:
            context = {
                "status":404,
                "message":f'Variation {id} does not exist.'
            }
            return Response(context,status=404)
        
        queryset.delete()
        context = {
            'status':200,
            'message':f"Variation {id} deleted successfully"
        }
        return Response(context,status=200)


class VariationUpdate(generics.UpdateAPIView):
    permission_classes = (IsAdmin | IsSuperAdmin | IsKitchenStaff,)
    queryset = Variation.objects.all()
    serializer_class = VariationUpdateSerializer
    filter_backends = [DjangoFilterBackend]

    def update(self, request,id, *args, **kwargs):
        try:
            variation = Variation.objects.get(id=id)
        except:
            context = {
                'status':404,
                "message":'Variation does not exists on database'
            }
            return Response(context,status=404)
        serializer = VariationUpdateSerializer(variation,data=request.data,partial=True)
        if serializer.is_valid():
            serializer.save()
            context = {
                'status':200,
                'message':"Updated variation successfully.",
                'variation':serializer.data
            }
            return Response(context, status=status.HTTP_200_OK)
        else:
            context = {
                'status':400,
                'message':'Entered data is not valid',
                'error':serializer.errors
            }
            return Response(context,status=400)


class Variationfilter(django_filters.FilterSet):
    name = django_filters.CharFilter(lookup_expr='icontains')
    
    class Meta:
        model = Variation
        fields = ['name']
   
class VariationFilter(viewsets.ModelViewSet):
    permission_classes = (IsAdmin | IsSuperAdmin | IsKitchenStaff,)
    queryset = Variation.objects.all()
    serializer_class = VariationSerializer
    filter_backends = [DjangoFilterBackend]
    filter_class = Variationfilter



class CategoryView(APIView):
    permission_classes = (IsAdmin | IsSuperAdmin | IsKitchenStaff,)
    def get(self, request, format=None):
        queryset = Category.objects.all()
        serializer = CategorySerializer(queryset, many=True)
        context = {
            "status":200,
            "message":"All Category got successfully.",
            "category":serializer.data
        }
        return Response(context,status=200)

    def post(self, request, format=None):
        
        serializer = CategoryCreateSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            context = {
                "status":201,
                "message":"Category created successfully",
                "category":serializer.data
            }
            return Response(context, status=status.HTTP_201_CREATED)
        else:
            context = {
                'status':400,
                "message":"Data entered is not valid",
                "errors":serializer.errors
            }
            return Response(context, status=status.HTTP_400_BAD_REQUEST)


class CategoryDetailView(APIView):
    permission_classes = (IsAdmin | IsSuperAdmin | IsKitchenStaff,)
    def get(self,request,id,format=None):

        try:
            queryset = Category.objects.filter(id=id)
            category = queryset[0]
        except:
            context = {
                "status":404,
                "message":f'Food item {id} does not exist.'
            }
            return Response(context,status=404)
        
        serializer = CategorySerializer(queryset,many=True)
        context = {
            "status":200,
            "message":f'Category {id} got successfully.',
            'category':serializer.data[0]
        }
        return Response(context,status=200)

    def delete(self,request,id,format=None):
        try:
            queryset = Category.objects.filter(id=id)
            category = queryset[0]
        except:
            context = {
                "status":404,
                "message":f'Category {id} does not exist.'
            }
            return Response(context,status=404)
        
        queryset.delete()
        context = {
            'status':200,
            'message':"Category deleted successfully"
        }
        return Response(context,status=200)

class CategoryCreateView(generics.UpdateAPIView):
    permission_classes = (IsAdmin | IsSuperAdmin | IsKitchenStaff,)
    queryset = Category.objects.all()
    serializer_class = CategoryCreateSerializer
    filter_backends = [DjangoFilterBackend]




    def update(self, request,id, *args, **kwargs):
        try:
            category = Category.objects.get(id=id)
        except:
            context = {
                'status':404,
                "message":'Category does not exists on database'
            }
            return Response(context,status=404)
        serializer = self.serializer_class(category, data=request.data,partial=True)
        if serializer.is_valid():
            serializer.save()
            context = {
                'status':200,
                'message':"Updated category successfully.",
                'category':serializer.data
            }
            return Response(context, status=status.HTTP_200_OK)
        else:
            context = {
                'status':400,
                'message':'Entered data is not valid',
                'error':serializer.errors
            }
            return Response(context,status=400)

class Categoryfilter(django_filters.FilterSet):
    name = django_filters.CharFilter(lookup_expr="icontains")

    class Meta:
        model = Category
        fields = ['name']


class CategoryFilter(viewsets.ModelViewSet):
    permission_classes = (IsAdmin | IsSuperAdmin | IsKitchenStaff,)
    queryset = Category.objects.all()
    serializer_class = CategorySerializer
    filter_backends = [DjangoFilterBackend]
    filter_class = Categoryfilter


class OrderItemView(APIView):
   
    
    def get(self, request, format=None):
        queryset = OrderItem.objects.all()
        serializer = OrderItemSerializer(queryset, many=True)
        context = {
            "status":200,
            "message":"All order item got successfully.",
            "order_item":serializer.data
        }
        return Response(context,status=200)

class OrderItemDetailView(APIView):
    def get(self,request,id,format=None):
        try:
            queryset = OrderItem.objects.filter(id=id)
            order_item = queryset[0]
        except:
            context = {
                "status":404,
                "message":f'Order item {id} does not exist.'
            }
            return Response(context,status=404)
        
        serializer = OrderItemSerializer(queryset,many=True)
        context = {
            "status":200,
            "message":f'Order item {id} got successfully.',
            'food_item':serializer.data[0]
        }
        return Response(context,status=200)

    def delete(self,request,id,format=None):
        try:
            queryset = OrderItem.objects.filter(id=id)
            order_item = queryset[0]
        except:
            context = {
                "status":404,
                "message":f'Order item {id} does not exist.'
            }
            return Response(context,status=404)
        
        queryset.delete()
        context = {
            'status':204,
            'message':"Food Item deleted successfully"
        }
        return Response(context,status=204)

class OrderItemFilterView(viewsets.ModelViewSet):
    queryset = OrderItem.objects.all()
    serializer_class = OrderItemSerializer
    filter_backends = [DjangoFilterBackend,SearchFilter]
    filter_class = OrderItemFilter
    permission_classes = (IsAdmin | IsKitchenStaff | IsSuperAdmin, )
    



    def retrieve(self, request, *args, **kwargs):
        food_item = self.get_queryset()
        serializer = self.serializer_class(food_item,many=True)
        context = {
            'status':200,
            'message':"food_item success.",
            'food_item':serializer.data
        }
        return Response(context, status=status.HTTP_200_OK)



class OrderView(APIView):
    permission_classes = (IsAdmin | IsSuperAdmin | IsKitchenStaff,)
    def get(self, request, format=None):
        queryset = Order.objects.all()
        serializer = OrderSerializer(queryset, many=True)
        context = {
            "status":200,
            "message":"All order item got successfully.",
            "order":serializer.data
        }
        return Response(context,status=200)


class OrderUserView(APIView):

    def get(self,request, id,*args, **kwargs):
        order = Order.objects.filter(profile=request.user.id)
        serializer = OrderDetailSerializer(order, many=True)
        context = {
            'status':200,
            'message':"Got user order successfully",
            'order':serializer.data
        }
        return Response(context,status=200)



class OrderDetailView(APIView):
    permission_classes = (IsAdmin | IsSuperAdmin ,)
    def get(self,request,id,format=None):
        try:
            queryset = Order.objects.filter(id=id)
            order_item = queryset[0]
        except:
            context = {
                "status":404,
                "message":f'Order {id} does not exist.'
            }
            return Response(context,status=404)
        
        serializer = OrderDetailSerializer(queryset,many=True)
        context = {
            "status":200,
            "message":f'Order {id} got successfully.',
            'order':serializer.data[0]
        }
        return Response(context,status=200)

    def delete(self,request,id,format=None):
        try:
            queryset = Order.objects.filter(id=id)
            order_ = queryset[0]
        except:
            context = {
                "status":404,
                "message":f'Order {id} does not exist.'
            }
            return Response(context,status=404)
        
        queryset.delete()
        context = {
            'status':200,
            'message':'Order deleted successfully'
        }
        return Response(context,status=204)


class OrderFilterView(viewsets.ModelViewSet):
    permission_classes = (IsAdmin | IsSuperAdmin | IsKitchenStaff,)
    queryset = Order.objects.all()
    serializer_class = OrderDetailSerializer
    filter_backends = [DjangoFilterBackend,SearchFilter]
    filter_class = OrderFilter
    


class OrdersPostView(APIView,IsAdmin,IsSuperAdmin,IsKitchenStaff):
    queryset = Order.objects.all()
    permission_classes = (IsSuperAdmin | IsAdmin | IsKitchenStaff,)
    serializer_class = OrderCreateSerializer
    
 
    def post(self,request):
        serializer = self.serializer_class(data=request.data)
       
        if serializer.is_valid():
            
            serializer.save()
            print(serializer.data['id'])
            order = Order.objects.filter(id=serializer.data['id'])
            ser = OrderSerializer(order,many=True)
            context = {
                "status":201,
                "message":"Order created successfully heh",
                "order":ser.data

            }
            return Response(context, status=status.HTTP_201_CREATED)
        else:
            context = {
                'status':400,
                "message":"Entered data is not valid",
                "error":serializer.errors
            }
            return Response(context,status=400)

            
