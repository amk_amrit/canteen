from rest_framework import  routers
from django.urls import  path,include
from . import views
from .views import  (
    FoodItemUpdateView,
    FoodItemDetailView,
    FoodItemView,
    CategoryView,
    CategoryCreateView,
    OrderItemView,
    OrderView,
    VariationView,
    CategoryDetailView,
    VariationDetailView,
    VariationUpdate,
    FoodItemFilter,
    OrderItemFilterView,
    VariationFilter,
    CategoryFilter,
    OrderFilterView,
    OrderDetailView
)


router = routers.DefaultRouter()
router.register('fooditem/filter',FoodItemFilter)
router.register('orderitem/filter',OrderItemFilterView)
router.register('category/filter',CategoryFilter)
router.register('variation/filter',VariationFilter)
router.register('order/filter',OrderFilterView)




urlpatterns = [
    path('',include(router.urls)),
    path('fooditem/',FoodItemView.as_view(),name="food_item_list"),
    path('fooditem/<id>/',FoodItemDetailView.as_view(),name="food_item_detail"),
    path('category/',CategoryView.as_view(),name="category_list"),
    path('category/<id>/',CategoryDetailView.as_view(),name="category_detial_view"),
    path('order/post/',views.OrdersPostView.as_view(), name="order_post_view"),
    path('order_item/',OrderItemView.as_view(),name="order_item_list"),
    path('order_item/<id>/',views.OrderItemDetailView.as_view(),name="order_item_detial"),
    path('order/',OrderView.as_view(),name="order_list"),
    path('order/<id>/',OrderDetailView.as_view(),name="order_detail"),
    path('variation/',VariationView.as_view(),name="variation_list"),
    path('variation/<id>/',VariationDetailView.as_view(),name="variation_detail"),
    path('fooditem/update/<id>/',FoodItemUpdateView.as_view(),name='fooditem_update'),
    path('variation/update/<id>/',VariationUpdate.as_view(),name="variation_update"),
    path('category/update/<id>/',CategoryCreateView.as_view(),name="category_update"),
    path('order/user/<id>/',views.OrderUserView.as_view(),name="order_user"),





]