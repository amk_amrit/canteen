from rest_framework import serializers
from .models import  FoodItem,Category,Order,OrderItem,Variation
from rest_framework.exceptions import  ValidationError

# from accounts.serializers import UserSerializer

class FoodItemListSerializer(serializers.ModelSerializer):
    class Meta:
        model = FoodItem
        fields =  ['id','name','slug','price','image','created','updated','category','variation']
        depth = 1 


class FoodItemSerializer(serializers.ModelSerializer):
    class Meta:
        model = FoodItem
        fields = '__all__'

    

class CategoryListSerializer(serializers.ModelSerializer):
    class  Meta:
        model = Category
        fields = ['id','name','slug','image','food_item']

class CategorySerializer(serializers.ModelSerializer):
    food_item = FoodItemSerializer(many=True)
    class  Meta:
        model = Category
        fields = ['id','name','slug','image','food_item']

class FoodCategorySerializer(serializers.ModelSerializer):
    class  Meta:
        model = Category
        fields = ['id','name','slug','image']


class VariationSerializer(serializers.ModelSerializer):
    class  Meta:
        model = Variation
        fields = ['id','name']

    def create(self,validated_data):
        return Variation.objects.create(**validated_data)

class VariationUpdateSerializer(serializers.ModelSerializer):
    
    class Meta:
        model = Variation
        fields = ['id','name']

    def update(self,instance,validated_data):
        instance.name = validated_data.get('name',instance.name)
        instance.save()
        return instance

class CategoryCreateSerializer(serializers.ModelSerializer):
    class Meta:
        model = Category
        fields = ['id','name','slug','image']
       
    def create(self,validated_data):
        return Category.objects.create(**validated_data)

class FoodItemCreateSerializer(serializers.ModelSerializer):
    
    
    class Meta:
        model = FoodItem   
        fields = ['id','name','slug','price','category','image','variation','created','updated']
        

    
    def update(self,instance,validated_data):
        instance.name = validated_data.get('name',instance.name)
        instance.slug = validated_data.get('slug',instance.slug)
        instance.price = validated_data.get('price',instance.price)
        instance.image = validated_data.get('image',instance.image)
        instance.category = validated_data.get('category',instance.category)
        instance.variation = validated_data.get('variation',instance.variation)
        instance.save()
        return instance
        
        

       
class OrderItemSerializer(serializers.ModelSerializer):
    total = serializers.ReadOnlyField()
    class Meta:
        model = OrderItem
        fields = ['id','profile','food_item','quantity','total','order_added_date','order_added_time']
        depth = 1
    


class OrderItemListSerializer(serializers.ModelSerializer):
    
    id = serializers.IntegerField(required = False)
    class Meta:
        model = OrderItem
        fields = ['id','profile','food_item','quantity','total','order_added_date','order_added_time']
    
    def create(self,validated_data):
        return OrderItem.objects.create(**validated_data)

        
    
class OrderSerializer(serializers.ModelSerializer):
    #grand_total = serializers.ReadOnlyField()
    order_item = OrderItemSerializer(many=True)
    class Meta:
        model = Order
        fields = ['id','profile','order_item','paid','order_created_date','order_created_time','grand_total']
        depth = 2
    
    



class OrderDetailSerializer(serializers.ModelSerializer):
   
    order_item = OrderItemListSerializer(many=True)
    #profile = ProfileSerializer(many=True)
    
    class Meta:
        model = Order
        fields = ['id','profile','order_item','paid','order_created_date','order_created_time','grand_total']
        depth = 1
        
        
    

class OrderCreateSerializer(serializers.ModelSerializer):
    
    order_item = OrderItemListSerializer(many=True)
    #grand_totall = serializers.SerializerMethodField()
    
    class Meta:
        model = Order
        fields = ['id','profile','order_item','paid','order_created_date','order_created_time','grand_total']

    def validate(self,attrs):
        if attrs['profile'].amount < attrs['grand_total']:
            raise  serializers.ValidationError({'amount':"You don't have enough balance"})  
        
        if attrs['paid'] != True:
            raise  serializers.ValidationError({'paid':"You must pay."})
        return attrs      
        
    def create(self, validated_data):
        order_items_data = validated_data.pop('order_item')
        new_order = Order.objects.create(**validated_data)
        order_item = []
        for item in order_items_data:
            order_item_id = item.pop('id',None)
            order_i,created = OrderItem.objects.get_or_create(id=order_item_id,defaults=item)
            order_item.append(order_i)
            
        new_order.order_item.set(order_item)
        
        return new_order



