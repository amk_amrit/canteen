from django.db import models
from django.conf import settings
from django.shortcuts import  reverse
from mptt.models import  MPTTModel
from treewidget.fields import TreeForeignKey,TreeManyToManyField
from accounts.models import User
from django.template.defaultfilters import slugify
from django.db.models.signals import  post_save
from django.dispatch import  receiver
import datetime

# Create your models here.
class Variation(models.Model):
    name = models.CharField(max_length=50,null=True,blank=True)

    def __str__(self):
        return self.name

class Category(models.Model):
    name = models.CharField(max_length=50)
    slug = models.SlugField(max_length=50,blank=True)
    image = models.ImageField(null=True,blank=True)

    # class Meta:
    #     permissions = (
    #         ('add_category', 'Add category'),
    #     )

    def __str__(self):
        return self.name 

    def save(self, *args, **kwargs):
        self.slug = slugify(self.name)
        super().save(*args, **kwargs)


class FoodItem(models.Model):
    category = models.ForeignKey(Category,related_name='food_item',on_delete=models.CASCADE)
    variation = models.ForeignKey(Variation,on_delete=models.CASCADE,null=True,blank=True)
    name = models.CharField(max_length=50)
    slug = models.SlugField(max_length=50,blank=True)
    price = models.DecimalField(max_digits=10,decimal_places=2)
    image = models.ImageField(null=True,blank=True)
    created = models.DateTimeField(auto_now_add=True)
    updated = models.DateTimeField(auto_now=True)

    class Meta:
        # ordering = ['id']
        index_together = (('id','slug'),)
       

    def __str__(self):
        return self.name

    def save(self, *args, **kwargs):
        self.slug = slugify(self.name)
        super().save(*args, **kwargs)




class OrderItem(models.Model):
    id = models.AutoField(primary_key=True)
    profile = models.ForeignKey(User,on_delete=models.CASCADE)
    food_item = models.ForeignKey(FoodItem,on_delete=models.CASCADE)
    quantity = models.IntegerField(default=1)
    order_added_date = models.DateField(auto_now_add=True)
    order_added_time = models.TimeField(auto_now_add=True)
    total = models.DecimalField(max_digits=10,decimal_places=2, null=True,blank=True)


    def __str__(self):
        return f'{self.quantity} of {self.food_item}'









class Order(models.Model):
    profile = models.ForeignKey(User,on_delete=models.CASCADE)
    order_item = models.ManyToManyField(OrderItem)
    order_created_time = models.TimeField(auto_now_add=True)
    order_created_date = models.DateField(auto_now_add=True)
    paid = models.BooleanField(default=False)
    grand_total = models.DecimalField(max_digits=10,decimal_places=2, null=True,blank=True)


    # @property
    # def token_id(self):
    #     token = 0
    #     for datetime.timedelta(minutes=1)
    #         token =+ 1,
    #     return token




    
@receiver(post_save,sender=Order)
def order_post_save(sender,instance,created,*args,**kwargs):
    if created:
        
        instance.profile.amount =  instance.profile.amount - instance.grand_total
        instance.profile.save()
        return instance

    

    

    
        



