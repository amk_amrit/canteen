from django import forms

from django.contrib.auth.forms import UserCreationForm, UserChangeForm

from .models import User


class UserCreationForms(UserCreationForm):

    class Meta(UserCreationForm):
        model = User
        fields = ('email',)


class UserChangeForms(UserChangeForm):

    class Meta:
        model = User
        fields = ('email',)
