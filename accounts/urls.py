from django.contrib import admin
from django.urls import path,re_path
from django.conf.urls import include,url
from . import views

from rest_framework import  routers
from .views import (
  StudentRegistration,
  EmployeeRegistration,
  KitchenStaffView,
  SuperAdminView,
  StudentDetailView,
  StudentUpdate,
  SuperAdminUpdate,
  AdminUpdate,
  KitchenStaffUpdate,
  AdminDetailView,
  KitchenStaffDetailView,
  SuperAdminDetailView,
  StudentFilter,
  UserFilter,
  StaffFilter,
  StudentPhotoUpdate,
  AdminPhotoUpdate,
  SuperAdminPhotoUpdate,
  KitchenStaffPhotoUpdate

)  
from rest_framework.authtoken import  views as authviews
from rest_auth.registration.views import VerifyEmailView, RegisterView
from rest_framework_jwt.blacklist.views import BlacklistView
router = routers.DefaultRouter()
router.register('student/filter',StudentFilter)
router.register('user/filter',UserFilter)
router.register('staff/filter',StaffFilter)



urlpatterns = [
  path('',include(router.urls)),
  path('api-auth/',include('rest_framework.urls',namespace="rest_framework")),
  path('student/',StudentRegistration.as_view(),name="student_view"),
  path('student/<id>/',StudentDetailView.as_view(),name="student_detail"),
  path('student/update/<id>/',StudentUpdate.as_view(),name="update"),
  path('student/photo/update/<id>/',StudentPhotoUpdate.as_view(),name="student_photo_update"),
  path('kitchenstaff/',KitchenStaffView.as_view(),name="kitchen_staff"),
  path('kitchenstaff/<id>/',KitchenStaffDetailView.as_view(),name="kitchen_staff_detail"),
  path('kitchenstaff/update/<id>/',KitchenStaffUpdate.as_view(),name="kitchen_update"),
  path('kitchenstaff/photo/update/<id>/',KitchenStaffPhotoUpdate.as_view(),name="kitchen_staff_photo_update"),
  path('superadmin/',SuperAdminView.as_view(),name="super_admin"),
  path('superadmin/<id>/',SuperAdminDetailView.as_view(),name="superadmin_detail"),
  path('superadmin/update/<id>/',SuperAdminUpdate.as_view(),name="superadmin_update"),
  path('superadmin/photo/update/<id>/',SuperAdminPhotoUpdate.as_view(),name="superadmin_photo_update"),
  path('admin/',EmployeeRegistration.as_view(),name="admin"),
  path('admin/<id>/',AdminDetailView.as_view(),name="admin_detail"),
  path('admin_update/<id>/',AdminUpdate.as_view(),name="admin_update"),
  path('admin/photo/update/<id>/',AdminPhotoUpdate.as_view(),name="admin_photo_update"),
  path('login/',views.authenticate_user,name="login"),
  path('s/<id>/',views.StudentRetriveView.as_view(),name="student_self_view"),
  path('a/<id>/',views.AdminRetriveView.as_view(),name="admin_self_view"),
  path('ks/<id>/',views.KitchenStaffRetriveView.as_view(),name="kitchenstaff_self_view"),
  path('sa/<id>/',views.SuperAdminRetriveView.as_view(),name="superadmin_self_view"),
  path('s/update/<id>/',views.StudentUpdateViewSelf.as_view(),name="student_self_update"),
  path('a/update/<id>/',views.AdminUpdateViewSelf.as_view(),name="admin_self_upadte"),
  path('ks/update/<id>/',views.KitchenStaffUpdateViewSelf.as_view(),name="kitchenstaff_self_update"),
  path('sa/update/<id>/',views.SuperAdminUpdateViewSelf.as_view(),name="superadmin_self_update"),
  path('s/phot/update/<id>',views.StudentPhotoUpdateSelfView.as_view(),name="student_photo_self_update"),
  path('a/photo/update/<id>/',views.AdminPhotoUpdateSelfView.as_view(),name="admin_photo_self_update"),
  path('ks/photo/update/<id>/',views.KitchenStaffPhotoUpdateSelfView.as_view(),name="kitchenstaff_photo_self_update"),
  path('sa/photo/update/<id>/',views.SuperAdminPhotoUpdateSelfView.as_view(),name="superadmin_photo_self_update"),
  #path('students/filter/',StudentFilter.as_view(),name="student_filter"),
  path('password_change/',views.ChangePasswordView.as_view(),name="password_change"),
  path('logout/',BlacklistView.as_view({'post':'create'})),
  

]

urlpatterns += [
  path('api-token-auth/', authviews.obtain_auth_token),
 

]