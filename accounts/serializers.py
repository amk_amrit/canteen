from rest_framework import  serializers,exceptions
from rest_framework.exceptions import ValidationError
from django.conf import settings
from django.utils.translation import ugettext_lazy as _
from rest_framework.response import  Response
from .models import  User,Student,Admin,SuperAdmin,KitchenStaff
from django.contrib.auth import authenticate,get_user_model
from django.contrib.auth.models import Permission,Group,update_last_login
from  django.core.exceptions import  ObjectDoesNotExist
from rest_auth.registration.serializers import RegisterSerializer



class StudentRegistrationSerializer(serializers.ModelSerializer):
    is_student = serializers.BooleanField(default=True)
    password = serializers.CharField(
        max_length=128,
        min_length=8,
        write_only=True
    )
    amount = serializers.DecimalField(max_digits = 10, decimal_places=2, default = 0)
    username = serializers.CharField(max_length=50,read_only=True)
    
 
    class Meta:
        model = Student
        fields = ['id','email','password','username','first_name','last_name','grade','section','academic_year','gender','amount','date_of_birth','phone_no','address','photo','father_name','father_email','father_phone_no','mother_name','mother_email','mother_phone_no','is_student']
 
    def create(self, validated_data):
        password = validated_data.pop('password')
        student = Student.objects.create(**validated_data)
        student.set_password(password)
        student.save()
        return student
 
class StudentPhoto(serializers.ModelSerializer):
    class Meta:
        model = Student
        fields = ['photo']

    def update(self,instance,validated_data):
        instance.photo = validated_data.get('photo',instance.photo)

class StudentUpdateSerializer(serializers.ModelSerializer):
    amount = serializers.DecimalField(max_digits = 10, decimal_places=2)
    class Meta:
        model = Student
        fields = ['id','username','first_name','last_name','grade','section','academic_year','gender','amount','date_of_birth','phone_no','address','father_name','father_email','father_phone_no','mother_name','mother_email','mother_phone_no']

    def update(self,instance,validated_data):
        instance.username = validated_data.get('username',instance.username)
        instance.first_name = validated_data.get('first_name',instance.first_name)
        instance.last_name = validated_data.get('last_name',instance.last_name)
        instance.grade = validated_data.get('grade',instance.grade)
        instance.section = validated_data.get('section',instance.section)
        instance.academic_year = validated_data.get('academic_year',instance.academic_year)
        instance.gender = validated_data.get('gender',instance.gender)
        instance.amount = validated_data.get('amount',instance.amount)
        instance.date_of_birth = validated_data.get('date_of_birth',instance.date_of_birth)
        instance.phone_no = validated_data.get('phone_no',instance.phone_no)
        instance.address = validated_data.get('address',instance.address)
        instance.father_name = validated_data.get('father_name',instance.father_name)
        instance.father_email = validated_data.get('father_email',instance.father_email)
        instance.father_phone_no = validated_data.get('father_phone_no',instance.father_phone_no)
        instance.mother_name = validated_data.get('mother_name',instance.mother_name)
        instance.mother_email = validated_data.get('mother_email',instance.mother_email)
        instance.mother_phone_no = validated_data.get('mother_phone_no',instance.mother_phone_no)
        instance.save()
        return instance

class StudentAdminUpdateSerializer(serializers.ModelSerializer):
    amount = serializers.DecimalField(max_digits = 10, decimal_places=2)
    class Meta:
        model = Student
        fields = ['id','username','email','first_name','last_name','grade','section','academic_year','gender','amount','date_of_birth','phone_no','address','father_name','father_email','father_phone_no','mother_name','mother_email','mother_phone_no']

    def update(self,instance,validated_data):
        instance.email = validated_data.get('email',instance.email)
        instance.username = validated_data.get('username',instance.username)
        instance.first_name = validated_data.get('first_name',instance.first_name)
        instance.last_name = validated_data.get('last_name',instance.last_name)
        instance.grade = validated_data.get('grade',instance.grade)
        instance.section = validated_data.get('section',instance.section)
        instance.academic_year = validated_data.get('academic_year',instance.academic_year)
        instance.gender = validated_data.get('gender',instance.gender)
        instance.amount = validated_data.get('amount',instance.amount)
        instance.date_of_birth = validated_data.get('date_of_birth',instance.date_of_birth)
        instance.phone_no = validated_data.get('phone_no',instance.phone_no)
        instance.address = validated_data.get('address',instance.address)
        instance.father_name = validated_data.get('father_name',instance.father_name)
        instance.father_email = validated_data.get('father_email',instance.father_email)
        instance.father_phone_no = validated_data.get('father_phone_no',instance.father_phone_no)
        instance.mother_name = validated_data.get('mother_name',instance.mother_name)
        instance.mother_email = validated_data.get('mother_email',instance.mother_email)
        instance.mother_phone_no = validated_data.get('mother_phone_no',instance.mother_phone_no)
        instance.save()
        return instance

class EmployeeRegistrationSerializer(serializers.ModelSerializer):
    is_staff = serializers.BooleanField(default=True)
    is_admin = serializers.BooleanField(default = True)
    password = serializers.CharField(
        max_length=128,
        min_length=8,
        write_only=True,
    )
    amount = serializers.DecimalField(max_digits = 10, decimal_places=2, default = 0)
 
    class Meta:
        model = Admin
        fields = ['id','email','password','username','first_name','last_name','phone_no','amount','gender','photo','address','date_of_birth','is_staff','is_admin']
 
    def create(self, validated_data):
        password = validated_data.pop('password')
        admin = Admin.objects.create(**validated_data)
        admin.set_password(password)
        admin.save()
        return admin


class AdminPhoto(serializers.ModelSerializer):
    class Meta:
        model = Student
        fields = ['photo']

    def update(self,instance,validated_data):
        instance.photo = validated_data.get('photo',instance.photo)



class AdminUpdateSerializer(serializers.ModelSerializer):
    amount = serializers.DecimalField(max_digits = 10, decimal_places=2)
    class Meta:
        model = Admin
        fields = ['id','username','email','first_name','last_name','gender','amount','date_of_birth','phone_no','address']

    def update(self,instance,validated_data):
        instance.email = validated_data.get('email',instance.email)
        instance.username = validated_data.get('username',instance.username)
        instance.first_name = validated_data.get('first_name',instance.first_name)
        instance.last_name = validated_data.get('last_name',instance.last_name)
        instance.gender = validated_data.get('gender',instance.gender)
        instance.amount = validated_data.get('amount',instance.amount)
        instance.date_of_birth = validated_data.get('date_of_birth',instance.date_of_birth)
        instance.phone_no = validated_data.get('phone_no',instance.phone_no)
        instance.address = validated_data.get('address',instance.address)
        instance.save()
        return instance

class AdminUpdateSerializerSelf(serializers.ModelSerializer):
    amount = serializers.DecimalField(max_digits = 10, decimal_places=2)
    class Meta:
        model = Admin
        fields = ['id','username','email','first_name','last_name','gender','amount','date_of_birth','phone_no','address']

    def update(self,instance,validated_data):
        
        instance.username = validated_data.get('username',instance.username)
        instance.first_name = validated_data.get('first_name',instance.first_name)
        instance.last_name = validated_data.get('last_name',instance.last_name)
        instance.gender = validated_data.get('gender',instance.gender)
        instance.amount = validated_data.get('amount',instance.amount)
        instance.date_of_birth = validated_data.get('date_of_birth',instance.date_of_birth)
        instance.phone_no = validated_data.get('phone_no',instance.phone_no)
        instance.address = validated_data.get('address',instance.address)
        instance.save()
        return instance


class KitchenStaffSerializer(serializers.ModelSerializer):
    is_staff = serializers.BooleanField(default=True)
    is_kitchenstaff = serializers.BooleanField(default=True)
    password = serializers.CharField(
        max_length=128,
        min_length=8,
        write_only=True
    )
    
    amount = serializers.DecimalField(max_digits = 10, decimal_places=2, default = 0)
    class Meta:
        model = KitchenStaff
        fields = ['id','email','password','username','first_name','last_name','amount','gender','phone_no','address','photo','date_of_birth','is_staff','is_kitchenstaff']
 
    def create(self, validated_data):
        password = validated_data.pop('password')
        kitchenstaff =  KitchenStaff.objects.create(**validated_data)
        kitchenstaff.set_password(password)
        kitchenstaff.save()
        return kitchenstaff


class KitchenStaffPhoto(serializers.ModelSerializer):
    class Meta:
        model = KitchenStaff
        fields = ['photo']

    def update(self,instance,validated_data):
        instance.photo = validated_data.get('photo',instance.photo)


class KitchenStaffUpdateSerializer(serializers.ModelSerializer):
    amount = serializers.DecimalField(max_digits = 10, decimal_places=2)
    class Meta:
        model = KitchenStaff
        fields = ['id','username','email','first_name','last_name','gender','amount','date_of_birth','phone_no','address']

    def update(self,instance,validated_data):
        instance.email = validated_data.get('email',instance.email)
        instance.username = validated_data.get('username',instance.username)
        instance.first_name = validated_data.get('first_name',instance.first_name)
        instance.last_name = validated_data.get('last_name',instance.last_name)
        instance.gender = validated_data.get('gender',instance.gender)
        instance.amount = validated_data.get('amount',instance.amount)
        instance.date_of_birth = validated_data.get('date_of_birth',instance.date_of_birth)
        instance.phone_no = validated_data.get('phone_no',instance.phone_no)
        instance.address = validated_data.get('address',instance.address)
        instance.save()
        return instance


class KitchenStaffUpdateSerializerSelf(serializers.ModelSerializer):
    amount = serializers.DecimalField(max_digits = 10, decimal_places=2)
    class Meta:
        model = KitchenStaff
        fields = ['id','username','email','first_name','last_name','gender','amount','date_of_birth','phone_no','address']

    def update(self,instance,validated_data):
        
        instance.username = validated_data.get('username',instance.username)
        instance.first_name = validated_data.get('first_name',instance.first_name)
        instance.last_name = validated_data.get('last_name',instance.last_name)
        instance.gender = validated_data.get('gender',instance.gender)
        instance.amount = validated_data.get('amount',instance.amount)
        instance.date_of_birth = validated_data.get('date_of_birth',instance.date_of_birth)
        instance.phone_no = validated_data.get('phone_no',instance.phone_no)
        instance.address = validated_data.get('address',instance.address)
        instance.save()
        return instance

class SuperAdminSerializer(serializers.ModelSerializer):
    amount = serializers.DecimalField(max_digits = 10, decimal_places=2, default = 0)
    is_superuser = serializers.BooleanField(default=True)
    password = serializers.CharField(
        max_length=128,
        min_length=8,
        write_only=True
    )
    
 
    class Meta:
        model = SuperAdmin
        fields = ['id','email','password','username','first_name','last_name','amount','gender','photo','phone_no','date_of_birth','is_superuser']
 
    def create(self, validated_data):
        password = validated_data.pop('password')
        superadmin =  SuperAdmin.objects.create(**validated_data)
        superadmin.set_password(password)
        superadmin.save()
        return superadmin

class SuperAdminPhoto(serializers.ModelSerializer):
    class Meta:
        model = SuperAdmin
        fields = ['photo']

    def update(self,instance,validated_data):
        instance.photo = validated_data.get('photo',instance.photo)

class SuperAdminUpdateSerializer(serializers.ModelSerializer):
    amount = serializers.DecimalField(max_digits = 10, decimal_places=2)
    class Meta:
        model = SuperAdmin
        fields = ['id','username','email','first_name','last_name','gender','amount','date_of_birth','phone_no','address']

    def update(self,instance,validated_data):
        instance.email = validated_data.get('email',instance.email)
        instance.username = validated_data.get('username',instance.username)
        instance.first_name = validated_data.get('first_name',instance.first_name)
        instance.last_name = validated_data.get('last_name',instance.last_name)
        instance.gender = validated_data.get('gender',instance.gender)
        instance.amount = validated_data.get('amount',instance.amount)
        instance.date_of_birth = validated_data.get('date_of_birth',instance.date_of_birth)
        instance.phone_no = validated_data.get('phone_no',instance.phone_no)
        instance.address = validated_data.get('address',instance.address)
        instance.save()
        return instance


class ChangePasswordSerializer(serializers.Serializer):
    model = User

    """
    Serializer for password change endpoint.
    """
    old_password = serializers.CharField(required=True)
    new_password = serializers.CharField(required=True)


    