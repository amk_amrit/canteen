from django.contrib.auth.models import (
    AbstractBaseUser, BaseUserManager, PermissionsMixin
)
from django.db import transaction


from django.utils.translation import ugettext_lazy as _


class UserManager(BaseUserManager):
    """
    Custom user model manager where email is the unique identifiers
    for authentication instead of usernames.
    """
    def create_user(self, email, password, **extra_fields):
        """
        Create and save a User with the given email and password.
        """
        if not email:
            raise ValueError(_('The Email must be set'))
        email = self.normalize_email(email)
        user = self.model(email=email, **extra_fields)
        user.set_password(password)
        user.save()
        return user

    def create_superuser(self, email, password, **extra_fields):
        """
        Create and save a SuperUser with the given email and password.
        """
        extra_fields.setdefault('is_staff', True)
        extra_fields.setdefault('is_superuser', True)
        extra_fields.setdefault('is_active', True)

        if extra_fields.get('is_staff') is not True:
            raise ValueError(_('Superuser must have is_staff=True.'))
        if extra_fields.get('is_superuser') is not True:
            raise ValueError(_('Superuser must have is_superuser=True.'))
        return self.create_user(email, password, **extra_fields)


class StudentManager(BaseUserManager):
 
    def create_student(self, first_name, last_name, email, password=None):
        if email is None:
            raise TypeError('Users must have an email address.')
        student = self.create_student(first_name=first_name, last_name=last_name, 
                          email=self.normalize_email(email),
                        )
        student.set_password(password)
        student.save()
        return student
 
 
class AdminManager(BaseUserManager):
 
    def create_admin(self, first_name, last_name, email, password):
        if email is None:
            raise TypeError('Users must have an email address.')
        admin = self.create_admin(first_name=first_name, last_name=last_name, 
                            email=self.normalize_email(email),
                        )
        admin.set_password(password)
        admin.save()
        return admin



class KitchenStaffManager(BaseUserManager):
 
    def create_kitchenstaff(self, first_name, last_name, email, password=None):
        if email is None:
            raise TypeError('Users must have an email address.')
        kitchenstaff = self.create_kitchenstaff(first_name=first_name, last_name=last_name, 
                            email=self.normalize_email(email),
                        )
        kitchenstaff.set_password(password)
        kitchenstaff.save()
        return kitchenstaff



class SuperAdminManager(BaseUserManager):
 
    def create_superadmin(self, first_name, last_name,is_superuser, email, password=None):
        if email is None:
            raise TypeError('Users must have an email address.')
        superadmin = self.create_superadmin(first_name=first_name, last_name=last_name, 
                            email=self.normalize_email(email),is_superuser=True,
                        )
        superadmin.set_password(password)
        superadmin.save()
        return superadmin

