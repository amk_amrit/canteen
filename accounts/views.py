from rest_framework.response import Response
from .serializers import  (
    StudentRegistrationSerializer,
    EmployeeRegistrationSerializer,
    KitchenStaffSerializer,
    SuperAdminSerializer,
    StudentUpdateSerializer,
    AdminUpdateSerializer,  
    KitchenStaffUpdateSerializer,
    SuperAdminUpdateSerializer,
    StudentPhoto,
    AdminPhoto, 
    KitchenStaffPhoto,
    SuperAdminPhoto,
    StudentAdminUpdateSerializer,
    AdminUpdateSerializerSelf,
    KitchenStaffUpdateSerializerSelf,
    ChangePasswordSerializer

)
from rest_framework.views import  APIView
from rest_framework.parsers import  JSONParser
from rest_framework.decorators import api_view,permission_classes
from rest_framework import status
from .models import  User,Student,Admin,KitchenStaff,SuperAdmin
from rest_framework import viewsets
from rest_framework import  generics
from rest_framework.authtoken.models import  Token
from rest_framework import  permissions
from django.contrib.auth.models import  Permission,Group
from rest_framework.renderers import JSONRenderer
from django_filters.rest_framework import DjangoFilterBackend
from django.http import Http404,JsonResponse,HttpResponse
from  django.core.exceptions import  ObjectDoesNotExist
from django.utils.decorators import method_decorator
from django.utils.translation import ugettext_lazy as _
from django.conf import settings
from django.views.decorators.csrf import ensure_csrf_cookie
from .utils import generate_access_token, generate_refresh_token
from rest_framework import exceptions
from django.contrib.auth import get_user_model
import json
from rest_framework_jwt.settings import api_settings
from .renders import  UserJSONRenderer
from rest_framework.filters import SearchFilter
import  jwt
from django.contrib.auth.signals import user_logged_in
from django.contrib.auth import authenticate
jwt_payload_handler = api_settings.JWT_PAYLOAD_HANDLER
jwt_encode_handler = api_settings.JWT_ENCODE_HANDLER
from rest_auth.registration.views import RegisterView
from allauth.account.models import EmailConfirmation, EmailConfirmationHMAC
from rest_framework.exceptions import NotFound
from django.http import HttpResponseRedirect
from rest_framework.permissions import BasePermission,SAFE_METHODS, DjangoModelPermissions,IsAdminUser,DjangoObjectPermissions,DjangoModelPermissions
from .permissions import  IsAdmin,IsSuperAdmin,IsKitchenStaff,IsStudent
import django_filters

class StudentRegistration(APIView,IsAdmin,IsSuperAdmin):
    permission_classes = ( IsAdmin | IsSuperAdmin,)
    queryset = Student.objects.all()
    
    #renderer_classes = (UserJSONRenderer,)
    serializer_class = StudentRegistrationSerializer

    def get(self,request):
        queryset = Student.objects.all()
        serializer = StudentRegistrationSerializer(queryset,many=True)
        context = {
            "status":200,
            "message":"All Student got successfully",
            "student":serializer.data
        }
        return Response(context,status=200)
 
    def post(self, request):
        serializer = self.serializer_class(data=request.data)
        if serializer.is_valid():
            serializer.save()
            context = {
                "status":201,
                "message":"Student created successfully",
                "student":serializer.data

            }
            return Response(context, status=status.HTTP_201_CREATED)
        else:
            context = {
                'status':400,
                "message":"Entered data is not valid",
                "error":serializer.errors
            }
            return Response(context,status=400)


class StudentDetailView(APIView):
    queryset = Student.objects.all()
    serializer_class = StudentUpdateSerializer
    filter_backends = [DjangoFilterBackend]
    permission_classes = (IsAdmin | IsSuperAdmin,)
    def get(self,request,id):
        try:
            queryset = Student.objects.filter(id=id)
            student = queryset[0]
        except :
            context = {
                "status":400,
                'message':f"Student with  {id} does not exists"
            }
            return Response(context,status=400)
        serializer = StudentRegistrationSerializer(queryset,many=True)
        context = {
            'status':200,
            'message':f"Student {id} got successfully.",
            'student': serializer.data
        }
        return Response(context,status=200)

    def delete(self,request,id):
        try:
            queryset = Student.objects.filter(id=id)
            student = queryset[0]
        except :
            context = {
                "status":400,
                'message':f"Student with id {id} does not exists"
            }
            return Response(context,status=400)
        
        queryset.delete()
        context = {
            'status':200,
            'message': f'Student {id} deleted successfully.'
        }
        return Response(context,status=200)

class StudentPhotoUpdate(generics.UpdateAPIView,IsAdmin,IsSuperAdmin):
    queryset = Student.objects.all()
    serializer_class = StudentPhoto
    filter_backends = [DjangoFilterBackend]
    permission_classes = (IsAdmin | IsSuperAdmin,)


    def update(self, request,id, *args, **kwargs):
        try:
            student = Student.objects.get(id=id)
        except:
            context = {
                'status':404,
                "message":'Student does not exists on database'
            }
            return Response(context,status=404)
        serializer = self.serializer_class(student, data=request.data,partial=True)
        if serializer.is_valid():
            serializer.save()
            context = {
                'status':200,
                'message':"Updated photo successfully.",
                'photo':serializer.data
            }
            return Response(context, status=status.HTTP_200_OK)
        else:
            context = {
                'status':400,
                'message':'Entered data is not valid',
                'error':serializer.errors
            }
            return Response(context,status=400)


class StudentPhotoUpdateSelfView(generics.UpdateAPIView):
    queryset = Student.objects.all()
    serializer_class = StudentPhoto
    filter_backends = [DjangoFilterBackend]
    


    def update(self, request,id, *args, **kwargs):
        
        student = Student.objects.filter(id=request.user.id)
        
        serializer = self.serializer_class(student, data=request.data,partial=True)
        if serializer.is_valid():
            serializer.save()
            context = {
                'status':200,
                'message':"Updated photo successfully.",
                'photo':serializer.data
            }
            return Response(context, status=status.HTTP_200_OK)
        else:
            context = {
                'status':400,
                'message':'Entered data is not valid',
                'error':serializer.errors
            }
            return Response(context,status=400)

class StudentUpdate(generics.UpdateAPIView,IsAdmin,IsSuperAdmin):
    queryset = Student.objects.all()
    serializer_class = StudentAdminUpdateSerializer
    filter_backends = [DjangoFilterBackend]
    permission_classes = (IsAdmin | IsSuperAdmin,)


    def update(self, request,id, *args, **kwargs):
        try:
            student = Student.objects.get(id=id)
        except:
            context = {
                'status':404,
                "message":'Student does not exists on database'
            }
            return Response(context,status=404)
        serializer = self.serializer_class(student, data=request.data,partial=True)
        if serializer.is_valid():
            serializer.save()
            context = {
                'status':200,
                'message':"Updated Student successfully.",
                'student':serializer.data
            }
            return Response(context, status=status.HTTP_200_OK)
        else:
            context = {
                'status':400,
                'message':'Entered data is not valid',
                'error':serializer.errors
            }
            return Response(context,status=400)


class StudentRetriveView(APIView):
    queryset = Student.objects.all()
    serializer_class = StudentUpdateSerializer
    filter_backends = [DjangoFilterBackend]
    

    def get(self,request,id):
    
        student = Student.objects.filter(id = request.user.id)
        serializer = StudentRegistrationSerializer(student,many=True)
        context = {
            'status':200,
            'message':f'Student {id} got successfully',
            'student':serializer.data
        }
        return Response(context,status=200)


class StudentUpdateViewSelf(generics.UpdateAPIView):
    queryset = Student.objects.all()
    serializer_class = StudentUpdateSerializer
    filter_backends = [DjangoFilterBackend]
    

    def update(self, request,id, *args, **kwargs):
        student = Student.objects.filter(id=request.user.id)
        serializer = self.serializer_class(student, data=request.data,partial=True)
        if serializer.is_valid():
            serializer.save()
            context = {
                'status':200,
                'message':"Updated Student successfully.",
                'student':serializer.data
            }
            return Response(context, status=status.HTTP_200_OK)
        else:
            context = {
                'status':400,
                'message':'Entered data is not valid',
                'error':serializer.errors
            }
            return Response(context,status=400)

class Studentfilter(django_filters.FilterSet):
    first_name = django_filters.CharFilter(lookup_expr='icontains')
    last_name = django_filters.CharFilter(lookup_expr='icontains')
    grade = django_filters.CharFilter(lookup_expr='iexact')
    academic_year = django_filters.CharFilter(lookup_expr = 'iexact')
    section = django_filters.CharFilter(lookup_expr = 'iexact')
    class Meta:
        model = Student
        fields = ['first_name','last_name','grade','section','academic_year']
    

class StudentFilter(viewsets.ModelViewSet):
    queryset = Student.objects.all()
    serializer_class = StudentRegistrationSerializer
    filter_backends = [DjangoFilterBackend]
    filter_class = Studentfilter
    

    

    
class EmployeeRegistration(APIView):
    permission_classes = (IsSuperAdmin,)
    #renderer_classes = (UserJSONRenderer,)
    serializer_class = EmployeeRegistrationSerializer

    def get(self,request):
        queryset = Admin.objects.all()
        serializer = EmployeeRegistrationSerializer(queryset,many=True)
        context = {
            "status":200,
            "message":"All Admin got successfully",
            "admin":serializer.data
        }
        return Response(context,status=200)


 
    def post(self, request):
        serializer = self.serializer_class(data=request.data)
        if serializer.is_valid():
            serializer.save()
            context = {
                "status":201,
                "message":"Admin created successfully",
                "admin":serializer.data

            }
            return Response(context, status=status.HTTP_201_CREATED)
        else:
            context = {
                'status':400,
                "message":"Entered data is not valid",
                "error":serializer.errors
            }
            return Response(context,status=400)

class AdminDetailView(APIView):
    permission_classes = (IsSuperAdmin,)
    def get(self,request,id):
        try:
            queryset = Admin.objects.filter(id=id)
            admin = queryset[0]
        except :
            context = {
                "status":400,
                'message':f"Admin with id {id} does not exists"
            }
            return Response(context,status=400)
        serializer = EmployeeRegistrationSerializer(queryset,many=True)
        context = {
            'status':200,
            'message':f"Admin {id} got successfully.",
            'admin': serializer.data
        }
        return Response(context,status=200)

    def delete(self,request,id):
        try:
            queryset = Admin.objects.filter(id=id)
            admin = queryset[0]
        except :
            context = {
                "status":400,
                'message':f"Admin with id {id} does not exists"
            }
            return Response(context,status=400)
        
        queryset.delete()
        context = {
            'status':200,
            'message': f'Admin {id} deleted successfully.'
        }
        return Response(context,status=200)

class AdminRetriveView(APIView):
    queryset = Admin.objects.all()
    serializer_class = EmployeeRegistrationSerializer
    filter_backends = [DjangoFilterBackend]
    

    def get(self,request,id):
    
        admin = Admin.objects.filter(id = request.user.id)
        serializer = EmployeeRegistrationSerializer(admin,many=True)
        context = {
            'status':200,
            'message':f'Admin {id} got successfully',
            'admin':serializer.data
        }
        return Response(context,status=200)

class AdminUpdateViewSelf(generics.UpdateAPIView):
    queryset = Admin.objects.all()
    serializer_class = AdminUpdateSerializerSelf
    filter_backends = [DjangoFilterBackend]
    

    def update(self, request,id, *args, **kwargs):
        admin = Admin.objects.filter(id=request.user.id)
        serializer = self.serializer_class(admin, data=request.data,partial=True)
        if serializer.is_valid():
            serializer.save()
            context = {
                'status':200,
                'message':"Updated admin successfully.",
                'admin':serializer.data
            }
            return Response(context, status=status.HTTP_200_OK)
        else:
            context = {
                'status':400,
                'message':'Entered data is not valid',
                'error':serializer.errors
            }
            return Response(context,status=400)


class AdminPhotoUpdate(generics.UpdateAPIView):
    permission_classes = (IsSuperAdmin,)
    queryset = Admin.objects.all()
    serializer_class = AdminPhoto
    filter_backends = [DjangoFilterBackend]

    def update(self, request,id, *args, **kwargs):
        try:
            admin = Admin.objects.get(id=id)
        except:
            context = {
                'status':404,
                "message":'Admin does not exists on database'
            }
            return Response(context,status=404)
        serializer = self.serializer_class(admin, data=request.data,partial=True)
        if serializer.is_valid():
            serializer.save()
            context = {
                'status':200,
                'message':"Updated photo successfully.",
                'photo':serializer.data
            }
            return Response(context, status=status.HTTP_200_OK)
        else:
            context = {
                'status':400,
                'message':'Entered data is not valid',
                'error':serializer.errors
            }
            return Response(context,status=400)


class AdminPhotoUpdateSelfView(generics.UpdateAPIView):
    queryset = Admin.objects.all()
    serializer_class = AdminPhoto
    filter_backends = [DjangoFilterBackend]

    def update(self, request,id, *args, **kwargs):
        
        admin = Admin.objects.filter(id=request.user.id)
        
        serializer = self.serializer_class(admin, data=request.data,partial=True)
        if serializer.is_valid():
            serializer.save()
            context = {
                'status':200,
                'message':"Updated photo successfully.",
                'photo':serializer.data
            }
            return Response(context, status=status.HTTP_200_OK)
        else:
            context = {
                'status':400,
                'message':'Entered data is not valid',
                'error':serializer.errors
            }
            return Response(context,status=400)


class AdminUpdate(generics.UpdateAPIView):
    permission_classes = (IsSuperAdmin,)
    queryset = Admin.objects.all()
    serializer_class = AdminUpdateSerializer
    filter_backends = [DjangoFilterBackend]

    def update(self, request,id, *args, **kwargs):
        try:
            admin = Admin.objects.get(id=id)
        except:
            context = {
                'status':404,
                "message":'Admin does not exists on database'
            }
            return Response(context,status=404)
        serializer = self.serializer_class(admin, data=request.data,partial=True)
        if serializer.is_valid():
            serializer.save()
            context = {
                'status':200,
                'message':"Updated Admin successfully.",
                'admin':serializer.data
            }
            return Response(context, status=status.HTTP_200_OK)
        else:
            context = {
                'status':400,
                'message':'Entered data is not valid',
                'error':serializer.errors
            }
            return Response(context,status=400)

 

class KitchenStaffView(APIView):
    permission_classes = (IsSuperAdmin | IsAdmin,)
    #renderer_classes = (UserJSONRenderer,)
    serializer_class = KitchenStaffSerializer

    def get(self,request):
        queryset = KitchenStaff.objects.all()
        serializer = KitchenStaffSerializer(queryset,many=True)
        context = {
            "status":200,
            "message":"All Admin got successfully",
            "kitchen_staff":serializer.data
        }
        return Response(context,status=200)

 
    def post(self, request):
        serializer = self.serializer_class(data=request.data)
        if serializer.is_valid():
            serializer.save()
            context = {
                "status":201,
                "message":"Kitchen Staff created successfully",
                "kitchen_staff":serializer.data

            }
            return Response(context, status=status.HTTP_201_CREATED)
        else:
            context = {
                'status':400,
                "message":"Entered data is not valid",
                "error":serializer.errors
            }
            return Response(context,status=400)


class KitchenStaffDetailView(APIView):
    permission_classes = (IsSuperAdmin | IsAdmin,)
    def get(self,request,id):
        try:
            queryset = KitchenStaff.objects.filter(id=id)
            kitchenstaff = queryset[0]
        except :
            context = {
                "status":400,
                'message':f"kitchenStaff with id {id} does not exists"
            }
            return Response(context,status=400)
        serializer = KitchenStaffSerializer(queryset,many=True)
        context = {
            'status':200,
            'message':f"kitchenStaff {id} got successfully.",
            'kitchen_staff': serializer.data
        }
        return Response(context,status=200)

    def delete(self,request,id):
        try:
            queryset = KitchenStaff.objects.filter(id=id)
            kitchenStaff = queryset[0]
        except :
            context = {
                "status":400,
                'message':f"kitchenStaff with id {id} does not exists"
            }
            return Response(context,status=400)
        
        queryset.delete()
        context = {
            'status':200,
            'message': f'kitchenStaff {id} deleted successfully.'
        }
        return Response(context,status=200)

class KitchenStaffRetriveView(APIView):
    queryset = KitchenStaff.objects.all()
    serializer_class = KitchenStaffSerializer
    filter_backends = [DjangoFilterBackend]
    

    def get(self,request,id):
    
        kitchenstaff = KitchenStaff.objects.filter(id = request.user.id)
        serializer = KitchenStaffSerializer(kitchenstaff,many=True)
        context = {
            'status':200,
            'message':f'kitchenstaff {id} got successfully',
            'kitchenstaff':serializer.data
        }
        return Response(context,status=200)


class KitchenStaffUpdateViewSelf(generics.UpdateAPIView):
    queryset = KitchenStaff.objects.all()
    serializer_class = KitchenStaffUpdateSerializerSelf
    filter_backends = [DjangoFilterBackend]
    

    def update(self, request,id, *args, **kwargs):
        kitchenstaff = KitchenStaff.objects.filter(id=request.user.id)
        serializer = self.serializer_class(kitchenstaff, data=request.data,partial=True)
        if serializer.is_valid():
            serializer.save()
            context = {
                'status':200,
                'message':"Updated kitchenstaff successfully.",
                'kitchenstaff':serializer.data
            }
            return Response(context, status=status.HTTP_200_OK)
        else:
            context = {
                'status':400,
                'message':'Entered data is not valid',
                'error':serializer.errors
            }
            return Response(context,status=400)


class KitchenStaffPhotoUpdate(generics.UpdateAPIView):
    permission_classes = (IsSuperAdmin | IsAdmin,)
    queryset = KitchenStaff.objects.all()
    serializer_class = KitchenStaffPhoto
    filter_backends = [DjangoFilterBackend]

    def update(self, request,id, *args, **kwargs):
        try:
            kitchenstaff = KitchenStaff.objects.get(id=id)
        except:
            context = {
                'status':404,
                "message":'Kitchen Staff does not exists on database'
            }
            return Response(context,status=404)
        serializer = self.serializer_class(kitchenstaff, data=request.data,partial=True)
        if serializer.is_valid():
            serializer.save()
            context = {
                'status':200,
                'message':"Updated photo successfully.",
                'photo':serializer.data
            }
            return Response(context, status=status.HTTP_200_OK)
        else:
            context = {
                'status':400,
                'message':'Entered data is not valid',
                'error':serializer.errors
            }
            return Response(context,status=400)

class KitchenStaffPhotoUpdateSelfView(generics.UpdateAPIView):
    queryset = KitchenStaff.objects.all()
    serializer_class = KitchenStaffPhoto
    filter_backends = [DjangoFilterBackend]

    def update(self, request,id, *args, **kwargs):
        
        kitchenstaff = KitchenStaff.objects.filter(id=request.user.id)
        
        serializer = self.serializer_class(kitchenstaff, data=request.data,partial=True)
        if serializer.is_valid():
            serializer.save()
            context = {
                'status':200,
                'message':"Updated photo successfully.",
                'photo':serializer.data
            }
            return Response(context, status=status.HTTP_200_OK)
        else:
            context = {
                'status':400,
                'message':'Entered data is not valid',
                'error':serializer.errors
            }
            return Response(context,status=400)



class KitchenStaffUpdate(generics.UpdateAPIView):
    permission_classes = (IsSuperAdmin | IsAdmin,)
    queryset = KitchenStaff.objects.all()
    serializer_class = KitchenStaffUpdateSerializer
    filter_backends = [DjangoFilterBackend]

    def update(self, request,id, *args, **kwargs):
        try:
            kitchenstaff = KitchenStaff.objects.get(id=id)
        except:
            context = {
                'status':404,
                "message":'Kitchen Staff does not exists on database'
            }
            return Response(context,status=404)
        serializer = self.serializer_class(kitchenstaff, data=request.data,partial=True)
        if serializer.is_valid():
            serializer.save()
            context = {
                'status':200,
                'message':"Updated KitchenStaff successfully.",
                'kitchen_staff':serializer.data
            }
            return Response(context, status=status.HTTP_200_OK)
        else:
            context = {
                'status':400,
                'message':'Entered data is not valid',
                'error':serializer.errors
            }
            return Response(context,status=400)

class SuperAdminView(APIView):
    permission_classes = (IsSuperAdmin,)
    #renderer_classes = (UserJSONRenderer,)
    serializer_class = SuperAdminSerializer

    def get(self,request):
        queryset = SuperAdmin.objects.all()
        serializer = SuperAdminSerializer(queryset,many=True)
        context = {
            "status":200,
            "message":"All Super Admin got successfully",
            "super_admin":serializer.data
        }
        return Response(context,status=200)

 
    def post(self, request):
        serializer = self.serializer_class(data=request.data)
        if serializer.is_valid():
            serializer.save()
            context = {
                "status":201,
                "message":"Super Admin created successfully",
                "super_admin":serializer.data

            }
            return Response(context, status=status.HTTP_201_CREATED)
        else:
            context = {
                'status':400,
                "message":"Entered data is not valid",
                "error":serializer.errors
            }
            return Response(context,status=400)


class SuperAdminDetailView(APIView):
    permission_classes = (IsSuperAdmin,)
    def get(self,request,id):
        try:
            queryset = SuperAdmin.objects.filter(id=id)
            superadmin = queryset[0]
        except :
            context = {
                "status":400,
                'message':f"SuperAdmin with id {id} does not exists"
            }
            return Response(context,status=400)
        serializer = SuperAdminSerializer(queryset,many=True)
        context = {
            'status':200,
            'message':f"SuperAdmin {id} got successfully.",
            'super_admin': serializer.data
        }
        return Response(context,status=200)

    def delete(self,request,id):
        try:
            queryset = SuperAdmin.objects.filter(id=id)
            superadmin = queryset[0]
        except :
            context = {
                "status":400,
                'message':f"SuperAdmin with id {id} does not exists"
            }
            return Response(context,status=400)
        
        queryset.delete()
        context = {
            'status':200,
            'message': f'SuperAdmin {id} deleted successfully.'
        }
        return Response(context,status=200)

class SuperAdminRetriveView(APIView):
    queryset = SuperAdmin.objects.all()
    serializer_class = SuperAdminSerializer
    filter_backends = [DjangoFilterBackend]
    

    def get(self,request,id):
    
        superadmin = SuperAdmin.objects.filter(id = request.user.id)
        serializer = SuperAdminSerializer(superadmin,many=True)
        context = {
            'status':200,
            'message':f'SuperAdmin {id} got successfully',
            'SuperAdmin':serializer.data
        }
        return Response(context,status=200)


class SuperAdminUpdateViewSelf(generics.UpdateAPIView):
    queryset = SuperAdmin.objects.all()
    serializer_class = AdminUpdateSerializer
    filter_backends = [DjangoFilterBackend]
    

    def update(self, request,id, *args, **kwargs):
        superadmin = SuperAdmin.objects.filter(id=request.user.id)
        serializer = self.serializer_class(superadmin, data=request.data,partial=True)
        if serializer.is_valid():
            serializer.save()
            context = {
                'status':200,
                'message':"Updated super admin successfully.",
                'super_admin':serializer.data
            }
            return Response(context, status=status.HTTP_200_OK)
        else:
            context = {
                'status':400,
                'message':'Entered data is not valid',
                'error':serializer.errors
            }
            return Response(context,status=400)


class Userfilter(django_filters.FilterSet):
    first_name = django_filters.CharFilter(lookup_expr="icontains")
    last_name = django_filters.CharFilter(lookup_expr="icontains")
    
    class Meta:
        model = User
        fields = ['first_name','last_name','is_admin','is_kitchenstaff','is_superuser']


class UserFilter(viewsets.ModelViewSet):
    permission_classes = (IsSuperAdmin,)
    queryset = User.objects.filter(is_staff=True)
    serializer_class = SuperAdminSerializer
    filter_backends = [DjangoFilterBackend]
    filter_class = Userfilter


    def retrieve(self, request, *args, **kwargs):
        super_admin = self.get_queryset()
        serializer = self.serializer_class(super_admin,many=True)
        context = {
            'status':200,
            'message':"Super Admin success.",
            'super_admin':serializer.data
        }
        return Response(context, status=status.HTTP_200_OK)


class SuperAdminPhotoUpdate(generics.UpdateAPIView):
    permission_classes = (IsSuperAdmin,)
    queryset = SuperAdmin.objects.all()
    serializer_class = SuperAdminUpdateSerializer
    filter_backends = [DjangoFilterBackend]

    def update(self, request,id, *args, **kwargs):
        try:
            superadmin = SuperAdmin.objects.get(id=id)
        except:
            context = {
                'status':404,
                "message":'SuperAdmin does not exists on database'
            }
            return Response(context,status=404)
        serializer = self.serializer_class(superadmin, data=request.data,partial=True)
        if serializer.is_valid():
            serializer.save()
            context = {
                'status':200,
                'message':"Updated photo successfully.",
                'photo':serializer.data
            }
            return Response(context, status=status.HTTP_200_OK)
        else:
            context = {
                'status':400,
                'message':'Entered data is not valid',
                'error':serializer.errors
            }
            return Response(context,status=400)


class SuperAdminPhotoUpdateSelfView(generics.UpdateAPIView):
    permission_classes = (IsSuperAdmin,)
    queryset = SuperAdmin.objects.all()
    serializer_class = SuperAdminUpdateSerializer
    filter_backends = [DjangoFilterBackend]

    def update(self, request,id, *args, **kwargs):
        superadmin = SuperAdmin.objects.filter(id=request.user.id)
        
        serializer = self.serializer_class(superadmin, data=request.data,partial=True)
        if serializer.is_valid():
            serializer.save()
            context = {
                'status':200,
                'message':"Updated photo successfully.",
                'photo':serializer.data
            }
            return Response(context, status=status.HTTP_200_OK)
        else:
            context = {
                'status':400,
                'message':'Entered data is not valid',
                'error':serializer.errors
            }
            return Response(context,status=400)



class SuperAdminUpdate(generics.UpdateAPIView):
    permission_classes = (IsSuperAdmin,)
    queryset = SuperAdmin.objects.all()
    serializer_class = SuperAdminUpdateSerializer
    filter_backends = [DjangoFilterBackend]

    def update(self, request,id, *args, **kwargs):
        try:
            superadmin = SuperAdmin.objects.get(id=id)
        except:
            context = {
                'status':404,
                "message":'SuperAdmin does not exists on database'
            }
            return Response(context,status=404)
        serializer = self.serializer_class(superadmin, data=request.data,partial=True)
        if serializer.is_valid():
            serializer.save()
            context = {
                'status':200,
                'message':"Updated Super Admin successfully.",
                'super_admin':serializer.data
            }
            return Response(context, status=status.HTTP_200_OK)
        else:
            context = {
                'status':400,
                'message':'Entered data is not valid',
                'error':serializer.errors
            }
            return Response(context,status=400)


class Stafffilter(django_filters.FilterSet):
    first_name = django_filters.CharFilter(lookup_expr="icontains")
    last_name = django_filters.CharFilter(lookup_expr="icontains")
    is_kitchenstaff = django_filters.BooleanFilter(lookup_expr="iexact")
    is_admin = django_filters.BooleanFilter(lookup_expr="iexact")

    class Meta:
        model = User
        fields = ['first_name','last_name','is_admin','is_kitchenstaff']
 
class StaffFilter(viewsets.ModelViewSet):
    permission_classes = (IsSuperAdmin | IsAdmin,)
    queryset = User.objects.filter(is_staff=True,is_superuser=False)
    serializer_class = SuperAdminSerializer
    filter_backends = [DjangoFilterBackend,SearchFilter]
    filter_class = Stafffilter

    def retrieve(self, request, *args, **kwargs):
        super_admin = self.get_queryset()
        serializer = self.serializer_class(super_admin,many=True)
        context = {
            'status':200,
            'message':"Super Admin success.",
            'staff':serializer.data
        }
        return Response(context, status=status.HTTP_200_OK)


@api_view(['POST'])
@permission_classes([permissions.AllowAny, ])
def authenticate_user(request):
 
    try:
        email = request.data['email']
        password = request.data['password']
 
        user = authenticate(email=email, password=password)
        if user:
            try:
                payload = jwt_payload_handler(user)
                token = jwt_encode_handler(payload)
                user_details = {}
                user_details['token'] = token
                user_details['id'] = user.id 
                user_details['email'] = user.email
                user_details['username'] = user.username
                user_details['first_name'] = user.first_name
                user_details['last_name'] = user.last_name
                user_details['gender'] = user.gender
                user_details['amount'] = str(user.amount)
                user_details['date_of_birth'] = user.date_of_birth
                user_details['address'] = user.address
                user_details['is_superuser'] = user.is_superuser
                user_details['is_student'] = user.is_student
                user_details['is_admin'] = user.is_admin
                user_details['is_kitchenstaff'] = user.is_kitchenstaff
                if user.is_admin:
                    user_details['father_name'] = None
                    user_details['father_email'] = None
                    user_details['father_phone_no'] = None
                    user_details['mother_name'] = None
                    user_details['mother_email'] = None
                    user_details['mother_phone_no'] = None
                if user.is_superuser:
                    user_details['father_name'] = None
                    user_details['father_email'] = None
                    user_details['father_phone_no'] = None
                    user_details['mother_name'] = None
                    user_details['mother_email'] = None
                    user_details['mother_phone_no'] = None
                if user.is_kitchenstaff:
                    user_details['father_name'] = None
                    user_details['father_email'] = None
                    user_details['father_phone_no'] = None
                    user_details['mother_name'] = None
                    user_details['mother_email'] = None
                    user_details['mother_phone_no'] = None
                if user.is_student:
                    student = Student.objects.filter(id = user.id)
                    ser = StudentRegistrationSerializer(student,many=True)
                    user_details['father_name'] = ser.data[0]['father_name']
                    user_details['father_email'] = ser.data[0]['father_email']
                    user_details['father_phone_no'] = ser.data[0]['father_phone_no']
                    user_details['mother_name'] = ser.data[0]['mother_name']
                    user_details['mother_email'] = ser.data[0]['mother_email']
                    user_details['mother_phone_no'] = ser.data[0]['mother_phone_no']
                user_logged_in.send(sender=user.__class__,
                                    request=request, user=user)
                context = {
                    'status':200,
                    'message':"User logged in successfully.",
                    'user':user_details
                }
                return Response(context, status=status.HTTP_200_OK)
 
            except Exception as e:
                raise e
        else:
            res = {
                'status':404,
                'message': 'can not authenticate with the given credentials or the account has been deactivated'}
            return Response(res, status=status.HTTP_403_FORBIDDEN)
    except KeyError:
        res = {
            'status':400,
            'error': 'please provide a email and a password'}
        return Response(res)



class ChangePasswordView(generics.UpdateAPIView):
    """
    An endpoint for changing password.
    """
    serializer_class = ChangePasswordSerializer
    model = User
    permission_classes = (permissions.IsAuthenticated,)

    def get_object(self, queryset=None):
        obj = self.request.user
        return obj

    def update(self, request, *args, **kwargs):
        self.object = self.get_object()
        serializer = self.get_serializer(data=request.data)

        if serializer.is_valid():
            # Check old password
            if not self.object.check_password(serializer.data.get("old_password")):
                return Response({"old_password": ["Wrong password."]}, status=status.HTTP_400_BAD_REQUEST)
            # set_password also hashes the password that the user will get
            self.object.set_password(serializer.data.get("new_password"))
            self.object.save()
            response = {
                'status': 'success',
                'code': status.HTTP_200_OK,
                'message': 'Password updated successfully'
                
            }

            return Response(response)

        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)