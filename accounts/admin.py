from django.contrib import admin
from django.contrib.auth.admin import UserAdmin
from django.utils.translation import ugettext_lazy as _
from .forms import UserCreationForms, UserChangeForms
from .models import User,Student,Admin,KitchenStaff,SuperAdmin


class Useradmin(UserAdmin):
    
    form = UserChangeForms
    add_form = UserCreationForms
    model = User
    list_display = ['email']
    fieldsets = (
        (None, {'fields': ('email', 'password')}),
        (_('Personal info'), {'fields': ('username','first_name', 'last_name', 'date_of_birth','address','amount')}),
        (_('Permissions'), {'fields': ('is_active', 'is_staff', 'is_superuser',
                                       'groups', 'user_permissions')}),
        (_('Important dates'), {'fields': ('last_login',)}),
    )
    add_fieldsets = (
        (None, {'fields': ('email', 'password')}),
        (_('Personal info'), {'fields': ('username','first_name', 'last_name', 'date_of_birth','address','amount')}),
        (_('Permissions'), {'fields': ('is_active', 'is_staff', 'is_superuser',
                                       'groups', 'user_permissions')}),
        (_('Important dates'), {'fields': ('last_login',)}),
    )


admin.site.register(User,Useradmin)
admin.site.register(Student)
admin.site.register(Admin)
admin.site.register(KitchenStaff)
admin.site.register(SuperAdmin)