from django.db import models
from django.conf import settings
from django.utils.translation import ugettext_lazy as _
from django.contrib.auth.models import AbstractUser
from django.db.models.signals import  post_save
from django.dispatch import  receiver
from rest_framework.authtoken.models import Token
from django_rest_resetpassword.signals import reset_password_token_created
from django.urls import  reverse
from  django.core.mail import EmailMultiAlternatives,send_mail
from django.template.loader import  render_to_string
from rest_framework.permissions import DjangoModelPermissions
from django.contrib.auth.models import Permission,Group
from datetime import  timedelta,datetime
import  jwt

from django.contrib.auth.models import (
    AbstractBaseUser, BaseUserManager, PermissionsMixin
)
from .managers import UserManager,StudentManager,AdminManager,KitchenStaffManager,SuperAdminManager
from django.utils import timezone

class User(AbstractBaseUser, PermissionsMixin):
   
    first_name = models.CharField(max_length=100)
    last_name = models.CharField(max_length=100)
    email = models.EmailField(db_index=True, unique=True)
    username = models.CharField(max_length=50,unique=True,null=True,blank=True)
    photo = models.ImageField(null=True,blank=True)
    amount = models.DecimalField(max_digits=10,decimal_places=2,default=0)
    date_of_birth = models.CharField(max_length = 15,null=True,blank=True)
    gender = models.CharField(max_length=10)
    phone_no = models.CharField(max_length=15)
    address = models.CharField(max_length=50,null=True,blank=True)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)
    is_active = models.BooleanField(default=True)
    is_staff = models.BooleanField(default=False)
    is_superuser = models.BooleanField(default=False)
    is_student = models.BooleanField(default=False)
    is_kitchenstaff = models.BooleanField(default=False)
    is_admin = models.BooleanField(default=False)
 
    USERNAME_FIELD = 'email'
    REQUIRED_FIELDS = ['first_name','is_staff','is_superuser']
    
    objects = UserManager()
 
    
 
    def __str__(self):
        return self.email
    
    def save(self,*args,**kwargs):
        self.username = self.first_name.lower() + str(self.id)
        return super(User,self).save(*args,**kwargs)

class Student(User, PermissionsMixin):
    grade = models.CharField(max_length=10)
    section = models.CharField(max_length=10)
    academic_year = models.CharField(max_length=10)
    mother_name = models.CharField(max_length=50,null=True,blank=True)
    mother_email = models.EmailField(max_length=50,null=True,blank=True)
    mother_phone_no = models.CharField(max_length=15,null=True,blank=True)
    father_name = models.CharField(max_length=50,null=True,blank=True)
    father_email = models.EmailField(max_length=50,null=True,blank=True)
    father_phone_no = models.CharField(max_length=15,null=True,blank=True)


    
    USERNAME_FIELD = 'email'
    REQUIRED_FIELDS = ['first_name', 'last_name']
 
    objects = StudentManager()
 
    def __str__(self):
        return self.email
 
class Admin(User, PermissionsMixin):
    
 
    USERNAME_FIELD = 'email'
    REQUIRED_FIELDS = ['first_name', 'last_name']
 
    objects = AdminManager()
 
    def __str__(self):
        return self.email


class KitchenStaff(User,PermissionsMixin):
    

    USERNAME_FIELD = 'email'
    REQUIRED_FIELDS = ['first_name','last_name']

    objects = KitchenStaffManager()

    def __str__(self):
        return self.email


class SuperAdmin(User,PermissionsMixin):

    USERNAME_FIELD = 'email'
    REQUIRED_FIELDS = ['first_name','last_name']

    objects = SuperAdminManager()

    def __str__(self):
        return self.email



            


# @receiver(post_save,sender=Student)
# def student_post_save(sender,instance,created,*args,**kwargs):
#     if created:
#         if instance.is_student == True:
            
#             instance.save()


@receiver(post_save,sender=Admin)
def admin_post_save(sender,instance,created,*args,**kwargs):
    if created:
        if instance.is_admin == True:
            permission = Permission.objects.get(codename='add_fooditem')
            permission1 = Permission.objects.get(codename='delete_fooditem')
            permission2 = Permission.objects.get(codename='view_fooditem')
            permission3 = Permission.objects.get(codename='change_fooditem')
            permission4 = Permission.objects.get(codename='add_student')
            permission5 = Permission.objects.get(codename='view_student')
            permission6 = Permission.objects.get(codename='delete_student')
            permission7 = Permission.objects.get(codename='change_student')
            permission8 = Permission.objects.get(codename='add_kitchenstaff')
            permission9 = Permission.objects.get(codename='view_kitchenstaff')
            permission10 = Permission.objects.get(codename='change_kitchenstaff')
            permission11 = Permission.objects.get(codename='delete_kitchenstaff')
            permission12 = Permission.objects.get(codename='add_orderitem')
            permission13 = Permission.objects.get(codename='view_orderitem')
            permission14 = Permission.objects.get(codename='change_orderitem')
            permission15 = Permission.objects.get(codename='delete_orderitem')
            permission16 = Permission.objects.get(codename='view_admin')
            permission17 = Permission.objects.get(codename='add_category')
            permission18 = Permission.objects.get(codename='view_category')
            permission19 = Permission.objects.get(codename='change_category')
            permission20 = Permission.objects.get(codename='delete_category')
            permission21 = Permission.objects.get(codename='add_variation')
            permission22 = Permission.objects.get(codename='change_variation')
            permission23 = Permission.objects.get(codename='view_variation')
            permission24 = Permission.objects.get(codename='delete_variation')
            permission25 = Permission.objects.get(codename='add_order')
            permission26 = Permission.objects.get(codename='view_order')
            permission27 = Permission.objects.get(codename='change_order')
            permission28 = Permission.objects.get(codename='delete_order')
            
            instance.user_permissions.add(permission,permission1,permission2,permission3,permission4,permission5,
            permission6,permission7,permission8,permission9,permission10,permission11,permission12,permission13,permission14,
            permission15,permission16,permission17,permission18,permission19,permission20,permission21,permission22,permission23,permission24,
            permission25,permission26,permission27,permission28)
            instance.save()


@receiver(post_save,sender=KitchenStaff)
def kitchen_post_save(sender,instance,created,*args,**kwargs):
    if created:
        if instance.is_kitchenstaff == True:
            permission = Permission.objects.get(codename='add_fooditem')
            permission19 = Permission.objects.get(codename='delete_fooditem')
            permission20 = Permission.objects.get(codename='view_fooditem')
            permission21 = Permission.objects.get(codename='change_fooditem')
            permission2 = Permission.objects.get(codename='add_orderitem')
            permission3 = Permission.objects.get(codename='view_orderitem')
            permission4 = Permission.objects.get(codename='change_orderitem')
            permission5 = Permission.objects.get(codename='delete_orderitem')
            permission7 = Permission.objects.get(codename='add_category')
            permission8 = Permission.objects.get(codename='view_category')
            permission9 = Permission.objects.get(codename='change_category')
            permission10 = Permission.objects.get(codename='delete_category')
            permission11 = Permission.objects.get(codename='add_variation')
            permission12 = Permission.objects.get(codename='change_variation')
            permission13 = Permission.objects.get(codename='view_variation')
            permission14 = Permission.objects.get(codename='delete_variation')
            permission15 = Permission.objects.get(codename='add_order')
            permission16 = Permission.objects.get(codename='view_order')
            permission17 = Permission.objects.get(codename='change_order')
            permission18 = Permission.objects.get(codename='delete_order')
            instance.user_permissions.add(permission,permission2,permission3,permission4,permission5,
            permission7,permission8,permission9,permission10,permission11,permission12,permission13,permission14,
            permission15,permission16,permission17,permission18,permission19,permission20,permission21)
            instance.save()


@receiver(post_save,sender=SuperAdmin)
def superadmin_post_save(sender,instance,created,*args,**kwargs):
    if created:
        if instance.is_superuser == True:
            permission = Permission.objects.all()
            instance.user_permissions.add(*permission)
            instance.save()



@receiver(reset_password_token_created)
def password_reset_token_created(sender,instance,reset_password_token,*args,**kwargs):
    email_plaintext_message = "token={} \n Go to this website for password reset confirm: {}".format(reset_password_token.key,('http://localhost:8000/api/password_reset/confirm/')
        )
    

    send_mail(
        # title:
        "Password Reset for {title}".format(title="Some website title"),
        # message:
        email_plaintext_message,
        
        # from:
        "noreply@somehost.local",
        # to:
        [reset_password_token.user.email]
    )


