from rest_framework import permissions 
from  rest_framework.permissions import  BasePermission,SAFE_METHODS
from rest_framework.response import  Response
 
class IsAdmin(BasePermission):
    message = {
        'status':403,
        "message":"You don't have permission for this"
    }
    def has_permission(self, request, view):
        
        return request.user.is_admin
        
class IsSuperAdmin(BasePermission):
    message = {
        'status':403,
        "message":"You don't have permission for this"
    }
    def has_permission(self, request, view):
        
        return request.user.is_superuser
        



class IsStudent(BasePermission):
    message = {
        'status':403,
        "message":"You don't have permission for this"
    }
    def has_permission(self, request, view):
       
        return request.user.is_student
        



class IsKitchenStaff(BasePermission):
    message = {
        'status':403,
        "message":"You don't have permission for this"
    }
    def has_permission(self, request, view):
        
        return request.user.is_kitchenstaff
        